---
layout: page
title: About
tags: [About]
modified: 2016-05-23T23:36:56+08:00
excerpt: All about forCodeSake!, how to get in touch and stay up-to-date.
comments: false
share: false
---

Coding adventures in Swift, iOS and OS X. 

### What's in a name?
It's obvious what `For Code Sake` rhymes with. Everyone has been there when coding!

### Contact
Each post has comments enabled. 

Otherwise, you can get in touch via Twitter [@_forCodeSake](https://twitter.com/_forCodeSake) or <a href="mailto:{{ 'web@forcodesake.me' | encode_email }}" title="email">email</a>. If you want to send a private email, you can make use of the [PGP key](https://keyserver.pgp.com/vkd/DownloadKey.event?keyid=0x9DC675B1FA3E2B10).


### Privacy
Google Analytics is used to track visitor numbers to this site and is subject to Google's [Privacy Policy](https://www.google.com/policies/privacy/). 
