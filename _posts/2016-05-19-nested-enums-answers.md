---
layout: post
title: Some Answers to the Question on Nested Enums
author: stuart_breckenridge 
feedauthor: "Stuart Breckenridge"
excerpt: "Various solutions to my question from a few days ago have been presented."
tags: [Nested Enums, Protocols]
topic: Swift
date: 2016-05-19T09:30:00+08:00
identifier: 05FAE948-7B52-4ABA-A92D-416394E15544
---

A [few days ago](https://forcodesake.me/question-on-nested-enums/) I asked if it was possible to write a function using the outermost enum to reference the nested enums. The example code:

```swift
enum ArmyRanks {
    enum Commissioned {
        case SecondLieutenant, Lieutenant, Captain
    }
    
    enum NonCommissioned {
        case Private, LanceCorporal, Corporal
    }
}

// Desired outcome:

struct Soldier {
    let rank:ArmyRanks
}

let s = Soldier(rank: .Commissioned.Captain) 
```

### Solution 1
Separate the outermost enum, make it a protocol, and make the nested enums conform to the protocol: 

```swift
protocol ArmyRanks {}

enum Commissioned:ArmyRanks {
        case SecondLieutenant, Lieutenant, Captain
    }
    
enum NonCommissioned:ArmyRanks {
        case Private, LanceCorporal, Corporal
    }
    
struct Soldier {
    let rank: ArmyRanks
}

let s = Soldier(rank: Commissioned.Captain)

switch soldier.rank
{
    case is Commissioned:
        print("Is a commissioned officer: \(soldier.rank)")
    case is NonCommissioned:
        print("Is a non-commissioned officer: \(soldier.rank)")
    default:
        break
}
```
    
### Solution 2
Add cases with associated values to the outermost enum:

```swift
enum Ranks{
    case Commissioned(CommissionedRanks)
    case NonCommissioned(NonCommissionedRanks)
    
    enum CommissionedRanks {
        case SecondLieutentant, Lieutentant, Captain
    }
    
    enum NonCommissionedRanks {
        case Private, LanceCorporal, Corporal
    }
}

struct Soldier {
    let rank:Ranks
}

let soldier = Soldier(rank: .Commissioned(.Captain))

switch soldier.rank
{
case .Commissioned(let description):
		print("Commissioned Officer: \(description)") // prints "Commissioned Officer: Captain"
case .NonCommissioned(let description):
		print("Non Commissioned Officer: \(description)")
}
```

The latter of these two solutions is my preferred approach. 

_Credit to @ryanbooker on the [Swift-Lang](http://swift-lang.schwa.io/) Slack channel for proposing both solutions._

    
    
    
    