---
layout: post
title: Default .gitignore for iOS and OS X Projects
author: stuart_breckenridge 
feedauthor: "forCodeSake()"
excerpt: "Wouldn't it be great if you could apply a default .gitignore when creating a new Xcode project?"
tags: [Git, Source Control]
topic: "Source Control"
date: 2016-05-06T23:09:09+08:00
identifier: 579496D8-93CC-4878-B277-3B73B0FE66D1
---

Whenever you start a new Xcode project you are presented with the option of creating a Git repository on your Mac or on a server. The issue with this is that you don't get a `.gitignore` file by default, so your initial commit is full of stuff you don't necessarily need, like [`.DS_Store`](http://stackoverflow.com/questions/16290054/ignoring-ds-store-in-a-gitignore-file-using-tower). There is an option to provide a default `.gitignore` when a new repository is initialised. 

First, open/create your `.gitconfig` file in Terminal: 

```bash
nano ~/.gitconfig
```

Add the following configuration details:

```bash
[core]
        excludesfile = ~/.gitignore
```

Save the file: `Ctrl + X`, `Y`, `Enter`

Then, open/create a new `.gitignore` at root level:

```bash
nano ~/.gitignore
```

Add the following configuration details to ensure that git intentionally will not track these files:

```bash
# Xcode
#
# gitignore contributors: remember to update Global/Xcode.gitignore, Objective-C.gitignore & Swift.gitignore

## Build generated
build/
DerivedData

## Various settings
*.pbxuser
!default.pbxuser
*.mode1v3
!default.mode1v3
*.mode2v3
!default.mode2v3
*.perspectivev3
!default.perspectivev3
xcuserdata

## Other
*.xccheckout
*.moved-aside
*.xcuserstate
*.xcscmblueprint

## Obj-C/Swift specific
*.hmap
*.ipa

## Playgrounds
timeline.xctimeline
playground.xcworkspace

# Swift Package Manager
#
# Add this line if you want to avoid checking in source code from Swift Package Manager dependencies.
# Packages/
.build/

# CocoaPods
#
# We recommend against adding the Pods directory to your .gitignore. However
# you should judge for yourself, the pros and cons are mentioned at:
# https://guides.cocoapods.org/using/using-cocoapods.html#should-i-check-the-pods-directory-into-source-control
#
# Pods/

# Carthage
#
# Add this line if you want to avoid checking in source code from Carthage dependencies.
# Carthage/Checkouts

Carthage/Build

# fastlane
#
# It is recommended to not store the screenshots in the git repo. Instead, use fastlane to re-generate the 
# screenshots whenever they are needed.
# For more information about the recommended setup visit:
# https://github.com/fastlane/fastlane/blob/master/docs/Gitignore.md

fastlane/report.xml
fastlane/screenshots

.DS_Store
```

Save the file: `Ctrl + X`, `Y`, `Enter`

Subsequent git repositories will read from this `.gitignore` file when being initialised. 

**Note:** If you wish to override the patterns in this `.gitignore`, you can add a local (to the repo) `.gitignore` that negates the pattern. For example, if you wanted to include `playground.xcworkspace` in the commit, you would add `!playground.xcworkspace` to the local `.gitignore` file.

_Recommended Reading_:

1. Git - gitignore Documentation (via [Git-SCM](https://git-scm.com/docs/gitignore/1.7.12)). 







