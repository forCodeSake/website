---
layout: post
title: Swift 3.0 Release Process
author: stuart_breckenridge 
feedauthor: "forCodeSake()"
excerpt: Later this year, Swift 3.0 will be released. It is source breaking and you should be aware of the release process. 
tags: [Swift 3]
topic: Swift
date: 2016-05-07T20:52:38+08:00
identifier: 3A1E2AA3-7647-410B-BB21-1FD32E57CBAB
---

From the [Swift Blog](https://swift.org/blog/swift-3-0-release-process/):

>Swift 3.0 is a major release that is not source-compatible with Swift 2.2. It contains fundamental changes to the language and Swift Standard Library. A comprehensive list of implemented changes for Swift 3.0 can be found on the [Swift evolution site](https://github.com/apple/swift-evolution#implemented-proposals-for-swift-3).
>
> ...
>
> Swift 3.0 is expected to be released sometime in late 2016. In addition to its Swift.org release, Swift 3.0 will ship in a future version of Xcode.

Exciting times. 
