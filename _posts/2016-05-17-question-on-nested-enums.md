---
layout: post
title: A Question on Nested Enums
excerpt: "Is it possible to write a function using the outermost enum to reference the nested enums?"
tags: [Nested Enums]
topic: Swift
date: 2016-05-17T23:21:26+08:00
modified: 2016-05-18T06:05:18+08:00
identifier: 6B6FA096-746B-4756-849A-D7795B0F9C17
---

I have a feeling the answer to this little experiment is _no_, but I thought I'd ask about it anyway.

Take the following example of a nested enumeration of commissioned and non-commissioned Army ranks:

```swift
enum ArmyRanks {
    enum Commissioned {
        case SecondLieutenant, Lieutenant, Captain
    }
    
    enum NonCommissioned {
        case Private, LanceCorporal, Corporal
    }
}
```

If I want to create a `struct` of a soldier using the above enum to denote rank, I could do this:

```swift
struct Soldier {
    let rank:ArmyRanks.Commissioned
}

//Soldier(rank: .Captain)
```

Or I could do this:

```swift
struct Soldier {
    let rank:ArmyRanks.NonCommissioned
}

//Soldier(rank: .Private)
```

I'd like to be able to write the struct referencing only `ArmyRanks` and then be able to provide `.Commissioned`, or `.NonCommissioned` as needed.

```swift
struct Soldier {
    let rank:ArmyRanks
}

//Soldier(rank: .Commissioned.Captain)
```

Is this possible?






