---
layout: post
title: Performance Analysis of appendIfUnique
author: stuart_breckenridge 
feedauthor: "forCodeSake()"
excerpt: "Analysing the performance of 'appendIfUnique' against the performance Array's 'contains' and Swift's 'Set' type." 
tags: [Swift, Array, Set, Performance]
date: 2016-05-02T22:00:43+08:00
modified: 2016-05-03T21:03:15+08:00
identifier: 91A74507-F2D3-4079-981A-E362083B135E
topic: Swift
#image:
#link:
---

[Last week](http://forcodesake.me/array-enforce-unique/) we looked at extending `Array` to include an `appendIfUnique` function. In this post—[inspired by feedback from Twitter](https://twitter.com/Kametrixom/status/726080489870753792)—we assess the performance of that function against the built in `contains(element: Self.Generator.Element)` and Swift's `Set` type. 

The difference between <code class="highlighter-rouge">O(n<sup>2</sup>)</code> and `O(n)` can be seen in this graph:

<figure>
    <img src="https://justin.abrah.ms/static/images/runtime_comparison.png"/>
    <center><figcaption>via <a href="https://justin.abrah.ms">justin.abrah.ms</a></figcaption></center>
</figure>

To see how close we are to <code class="highlighter-rouge">O(n<sup>2</sup>)</code> I've created three test cases: 

- one will insert `n` elements to a `Set` and then create an `Array` from that set;
- one will append `n` unique elements to an `Array` using `appendIfUnique`; and,
- one will append `n` unique elements to an `Array` using the `contains(element:Self.Generator.Element)` function


```swift
struct Benchmarks { 
    static func testNumbersSetToArray(size:Int) {
        var set = Set<Int>()
        
        while set.count < size {
            set.insert(set.count + 1)
        }
        
        let _ = Array(set)
    }
    
    static func testAppendIfUnique(size:Int) {
        var numbers = [Int]()
        
        while numbers.count < size {
            numbers.appendIfUnique(numbers.count + 1)
        }
    }
    
    static func testArrayContains(size:Int) {
        var numbers = [Int]()
        
        while numbers.count < size {
            if !numbers.contains(numbers.count + 1)
            {
                numbers.append(numbers.count + 1)
            }
        }
    }
}
```

Function performance is measured from `XCTestCase` by calling:

```swift
    func testAppend() {
        self.measureBlock {
            Benchmarks.testAppendIfUnique(60000)
        }
    }
    
    func testArrayContains(){
        self.measureBlock {
            Benchmarks.testArrayContains(60000)
        }
    }
    
    func testSet(){
        self.measureBlock {
            Benchmarks.testNumbersSetToArray(60000)
        }
    }
```

I ran tests with `n` going up to 60,000 and the compiler optimisation level set to `Fast[-0] Whole Module Optimization`. The results are below:

| Elements        | `appendIfUnique` | `contains`| `Set`     |
| -------------   | :-------------:  | :-----:   | :-------: |
| 10000           | 0.015s           | 0.029s    |  0.002s   |
| 20000           | 0.064s           | 0.112s    |  0.003s   |
| 30000           | 0.141s           | 0.251s    |  0.004s   |
| 40000           | 0.243s           | 0.450s    |  0.004s   |
| 50000           | 0.421s           | 0.695s    |  0.006s   |
| 60000           | 0.607s           | 1.019s    |  0.007s   | 

<figure>
<img src="https://storage.googleapis.com/fcsassets/images/appendIfUnique.png"/>
<center><figcaption>Performance Results</figcaption></center>
</figure>

**Conclusion**: Using `Set` is an outright winner, while `appendIfUnique` and `contains` are in line with <code class="highlighter-rouge">O(n<sup>2</sup>)</code>. `contains` is, however, considerably slower than `appendIfUnique` which is surprising.

The source code for these tests is available on [GitHub](https://github.com/forCodeSakeExamples/appendifunique-performance.git).

_Recommended Reading:_

- Collection Data Structures in Swift (via [Ray Wenderlich](https://www.raywenderlich.com/79850/collection-data-structures-swift)) 
- Whole Module Optimization (via [Use Your Loaf](http://useyourloaf.com/blog/swift-whole-module-optimization/)) 
- Collections (via [Objc.io](https://www.objc.io/issues/7-foundation/collections/))
