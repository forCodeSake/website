---
layout: post
title: "Hello, forCodeSake!"
author: stuart_breckenridge 
feedauthor: "Stuart Breckenridge"
excerpt: "Introducing forCodeSake!. A blog on coding in Swift, iOS, and OS X."
tags: [Hello, Swift, iOS, OS X]
date: 2016-04-28T19:07:25+08:00
modified: 2016-05-16T13:16:25+08:00
identifier: E6FEDA23-25A1-4E4E-AE04-FF88721F3C7A

image: hello.png
#link:
---

This blog will primarily focus on Swift and the frameworks that make up the iOS and OS X ecosystems and what better way to start it off than to use the tried, tested, and somewhat boring example of "Hello, World"?

```swift
func hello() {
	print("Hello, forCodeSake!.")
}

hello() // prints "Hello, forCodeSake!."
```


Every book in the world will high-five you about now and tell you you've just written your first program. Well done!

I intend to write a few articles every month and with WWDC around the corner, there should be a lot to write about!

