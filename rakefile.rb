# Ping Pingomatic
desc 'Ping pingomatic'
task :pingomatic do
  begin
    require 'xmlrpc/client'
    puts '* Pinging ping-o-matic'
    XMLRPC::Client.new('rpc.pingomatic.com', '/').call('weblogUpdates.extendedPing', 'forcodesake.me' , 'http://forcodesake.me', 'http://feedpress.me/forcodesake')
  rescue LoadError
    puts '! Could not ping ping-o-matic, because XMLRPC::Client could not be found.'
  end
end

# Ping Google
desc 'Notify Google of the new sitemap'
task :sitemapgoogle do
  begin
    require 'net/http'
    require 'uri'
    puts '* Pinging Google about our sitemap'
    Net::HTTP.get('www.google.com', '/webmasters/tools/ping?sitemap=' + URI.escape('http://forcodesake.me/sitemap.xml'))
  rescue LoadError
    puts '! Could not ping Google about our sitemap, because Net::HTTP or URI could not be found.'
  end
end

# Ping Bing
desc 'Notify Bing of the new sitemap'
task :sitemapbing do
  begin
    require 'net/http'
    require 'uri'
    puts '* Pinging Bing about our sitemap'
    Net::HTTP.get('www.bing.com', '/webmaster/ping.aspx?siteMap=' + URI.escape('http://forcodesake.me/sitemap.xml'))
  rescue LoadError
    puts '! Could not ping Bing about our sitemap, because Net::HTTP or URI could not be found.'
  end
end

# Ping pubsubhubbub
desc 'Notify pubsubhubbub server.'
task :pingpubsubhubbub do
  begin
    require 'cgi'
    require 'net/http'
    puts '* Pinging pubsubhubbub server'
    data = 'hub.mode=publish&hub.url=' + CGI::escape("http://feedpress.me/forcodesake")
    http = Net::HTTP.new('pubsubhubbub.appspot.com', 80)
    resp, data = http.post('http://pubsubhubbub.appspot.com/publish',
                           data,
                           {'Content-Type' => 'application/x-www-form-urlencoded'})
    puts "Ping error: #{resp}, #{data}" unless resp.code == "204"
  end
end # task: pubsubhubbub

# Insert this code in your Rakefile after the deploy task
desc "Ping FeedPress to notify about new content"
task :ping do
  require 'httparty'

  # Ping FeedPress to notify of updated feed
  # Add the FeedPress key and token as environmental variables in your shell 
  #   via a ~/.secrets file that you source in .bashrc or .zshrc
  # Specify urilv_name (short feed code). When you visit http://feedpress.it/feeds
  #   this is what is listed in the table under the heading 'Code'
  mykey       = "5726e7f141a6e"
  mytoken     = "5726e8039bb40"  
  urilv_name  = "forcodesake" # the short feed code for your feed in FeedPress
  
  ping = HTTParty.get( 'http://api.feedpress.it/feeds/ping.json', {:query => {:key => mykey, :token => mytoken, :feed => urilv_name}} )

  if ping.code == 200
    puts "==> Pinged FeedPress successfully. #{ping['message']}"
  else
    puts "Error: Ping rejected (#{ping.code} - #{ping.message})"
  end

end

# rake notify
desc 'Notify various services about new content'
task :notify => [:pingomatic, :sitemapgoogle, :sitemapbing, :pingpubsubhubbub, :ping] do
end